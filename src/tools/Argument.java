package tools;

import java.io.Serializable;

public class Argument implements Serializable
{
    String name;
    Variable value;

    public Argument(String name)
    {
        this.name = name;
        this.value = null;
    }

    public String getName()
    {
        return name;
    }

    public Variable getVariable()
    {
        return value;
    }
}
