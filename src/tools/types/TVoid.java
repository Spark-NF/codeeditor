package tools.types;

import java.awt.*;

public class TVoid extends Type
{
    protected static TVoid instance = null;

    protected TVoid()
    {
        color = new Color(192, 192, 192, 255);
    }
    public static TVoid getInstance()
    {
        if (instance == null)
            instance = new TVoid();
        return instance;
    }

    @Override
    public <T> void set(T value)
    {
        System.err.println("Trying to set void.");
    }
    @Override
    public Type get()
    {
        System.err.println("Trying to get void.");
        return null;
    }
}
