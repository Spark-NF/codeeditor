package tools.types;

import java.awt.*;

public class TBoolean extends Type
{
    protected static TBoolean instance = null;
    protected Boolean value;

    protected TBoolean()
    {
        color = new Color(192, 64, 64, 255);
    }
    public static TBoolean getInstance()
    {
        if (instance == null)
            instance = new TBoolean();
        return instance;
    }

    @Override
    public <T> void set(T value)
    {
        if (value instanceof Boolean)
            this.value = (Boolean)value;
    }
    @Override
    public Boolean get()
    {
        return value;
    }
}
