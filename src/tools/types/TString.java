package tools.types;

import java.awt.*;

public class TString extends Type
{
    protected static TString instance = null;
    protected String value;

    protected TString()
    {
        color = new Color(192, 128, 64, 255);
    }
    public static TString getInstance()
    {
        if (instance == null)
            instance = new TString();
        return instance;
    }

    @Override
    public <T> void set(T value)
    {
        if (value instanceof String)
            this.value = (String)value;
    }
    @Override
    public String get()
    {
        return value;
    }
}
