package tools.types;

import java.awt.*;

public class TInteger extends Type
{
    protected static TInteger instance = null;
    protected Integer value;

    protected TInteger()
    {
        color = new Color(192, 192, 64, 255);
    }
    public static TInteger getInstance()
    {
        if (instance == null)
            instance = new TInteger();
        return instance;
    }

    @Override
    public <T> void set(T value)
    {
        if (value instanceof Integer)
            this.value = (Integer)value;
    }
    @Override
    public Integer get()
    {
        return value;
    }
}
