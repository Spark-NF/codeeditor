package tools.types;

import java.awt.*;
import java.io.Serializable;

public abstract class Type implements Serializable
{
    Color color;

    public abstract <T> void set(T value);
    public abstract <T> T get();

    public Color getColor()
    {
        return color;
    }
}
