package tools.exps;

import tools.Argument;
import tools.Parameter;
import tools.types.*;

import java.awt.*;

public class GetExp extends Exp
{
    public GetExp()
    {
        super("Get");

        addOutput(new Parameter(this, "Out", TBoolean.getInstance()));
        addArgument(new Argument("Variable"));

        colorBorder = new Color(32, 32, 128, 255);
        colorBackground = new Color(32, 32, 64, 255);
    }

    @Override
    public Type evaluate()
    {
        return getArgument("Variable").getVariable().getValue();
    }

    @Override
    public String getName()
    {
        if (getArgument("Variable").getVariable() != null)
            return name + " '" + getArgument("Variable").getVariable().getName() + "'";
        return name;
    }
}
