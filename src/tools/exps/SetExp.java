package tools.exps;

import tools.Argument;
import tools.Parameter;
import tools.types.TBoolean;
import tools.types.TVoid;
import tools.types.Type;

import java.awt.*;

public class SetExp extends Exp
{
    public SetExp()
    {
        super("Set");

        addInput(new Parameter(this, "In", TBoolean.getInstance()));
        addArgument(new Argument("Variable"));

        colorBorder = new Color(32, 32, 128, 255);
        colorBackground = new Color(32, 32, 64, 255);
    }

    @Override
    public Type evaluate()
    {
        return TVoid.getInstance();
    }

    @Override
    public String getName()
    {
        if (getArgument("Variable").getVariable() != null)
            return name + " '" + getArgument("Variable").getVariable().getName() + "'";
        return name;
    }
}
