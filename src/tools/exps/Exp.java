package tools.exps;

import tools.Argument;
import tools.Parameter;
import tools.Variable;
import tools.types.TVoid;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Exp implements Serializable
{
    String name;
    ArrayList<Parameter> inputs;
    ArrayList<Parameter> outputs;
    ArrayList<Argument> arguments;
    int x;
    int y;
    int width;
    int height;
    boolean isSelected;
    protected Color colorBorder;
    protected Color colorBackground;

    public Exp(String name)
    {
        this(name, null, null, null);
    }
    public Exp(String name, ArrayList<Parameter> ins, ArrayList<Parameter> outs, ArrayList<Argument> vars)
    {
        this.name = name;

        inputs = ins == null ? new ArrayList<Parameter>() : ins;
        outputs = outs == null ? new ArrayList<Parameter>() : outs;
        arguments = vars == null ? new ArrayList<Argument>() : vars;

        colorBorder = new Color(32, 32, 128, 255);
        colorBackground = new Color(32, 32, 64, 255);
    }

    public void setPosition(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public void setDimension(int w, int h)
    {
        width = w;
        height = h;
    }

    public String getName()
    {
        return name;
    }

    public void addInput(Parameter parameter)
    {
        inputs.add(parameter);
    }
    public void addOutput(Parameter parameter)
    {
        outputs.add(parameter);
    }
    public void addArgument(Argument argument)
    {
        arguments.add(argument);
    }

    public Parameter getInput(String name)
    {
        for (Parameter p : inputs)
            if (p.getName().equals(name))
                return p;
        return null;
    }
    public Parameter getOutput(String name)
    {
        for (Parameter p : outputs)
            if (p.getName().equals(name))
                return p;
        return null;
    }
    public Argument getArgument(String name)
    {
        for (Argument v : arguments)
            if (v.getName().equals(name))
                return v;
        return null;
    }

    public void draw(Graphics2D graphics, int decX, int decY)
    {
        width = 120;
        height = 22 + 18 * Math.max(inputs.size(), outputs.size());

        graphics.setColor(colorBorder);
        graphics.fillRoundRect(x - decX, y - decY, width, height, 12, 12);
        graphics.setColor(colorBackground);
        graphics.drawRoundRect(x - decX, y - decY, width, height, 12, 12);

        if (!inputs.isEmpty())
        {
            graphics.setColor(new Color(192, 192, 192, 255));
            graphics.fillOval(x - decX + 5, y - decY + 7, 7, 7);
        }
        graphics.setColor(new Color(255, 255, 255, 255));
        graphics.drawString(getName(), x - decX + 17, y - decY + 15);

        int i = 0;
        for (Parameter p : inputs)
        {
            graphics.setColor(p.getType().getColor());
            graphics.fillOval(x - decX + 5, y - decY + 27 + i * 18, 7, 7);
            graphics.setColor(new Color(255, 255, 255, 255));
            graphics.drawString(p.getName(), x - decX + 17, y - decY + 35 + i * 18);
            ++i;
        }

        i = 0;
        boolean isControl = true;
        for (Parameter p : outputs)
        {
            graphics.setColor(p.getType().getColor());
            graphics.fillOval(x + width - decX - 12, y - decY + 27 + i * 18, 7, 7);
            graphics.setColor(new Color(255, 255, 255, 255));
            graphics.drawString(p.getName(), x + width - graphics.getFontMetrics().stringWidth(p.getName()) - decX - 15, y - decY + 35 + i * 18);
            if (p.getType() != TVoid.getInstance())
                isControl = false;
            ++i;
        }
        if (!isControl && !inputs.isEmpty())
        {
            graphics.setColor(new Color(192, 192, 192, 255));
            graphics.fillOval(x + width - decX - 12, y - decY + 7, 7, 7);
        }
    }

    public abstract tools.types.Type evaluate();

    public void setSelected(boolean isSelected)
    {
        this.isSelected = isSelected;
    }
    public boolean isSelected()
    {
        return isSelected;
    }
}
