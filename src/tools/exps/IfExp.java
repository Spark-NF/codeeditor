package tools.exps;

import tools.Parameter;
import tools.types.*;

import java.awt.*;

public class IfExp extends Exp
{
    public IfExp()
    {
        super("If");

        addInput(new Parameter(this, "Condition", TBoolean.getInstance()));
        addOutput(new Parameter(this, "Then", TVoid.getInstance()));
        addOutput(new Parameter(this, "Else", TVoid.getInstance()));

        colorBorder = new Color(32, 32, 128, 255);
        colorBackground = new Color(32, 32, 64, 255);
    }

    @Override
    public Type evaluate()
    {
        return null;
    }
}
