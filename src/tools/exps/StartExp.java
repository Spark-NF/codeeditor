package tools.exps;

import tools.Parameter;
import tools.types.*;

import java.awt.*;

public class StartExp extends Exp
{
    public StartExp()
    {
        super("Start");

        addOutput(new Parameter(this, "Action", TVoid.getInstance()));

        colorBorder = new Color(32, 32, 128, 255);
        colorBackground = new Color(32, 32, 64, 255);
    }

    @Override
    public Type evaluate()
    {
        return null;
    }
}
