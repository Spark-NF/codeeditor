package tools;

import tools.exps.Exp;
import tools.types.Type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Parameter implements Serializable
{
    Exp parent;
    String name;
    Type type;
    List<Parameter> links;

    public Parameter(Exp parent, String name, Type type)
    {
        this.parent = parent;
        this.name = name;
        this.type = type;

        links = new ArrayList<Parameter>();
    }

    public void removeLink(Parameter other)
    {
        links.remove(other);
    }
    public void addLink(Parameter other)
    {
        links.add(other);
    }
    public List<Parameter> getLinks()
    {
        return links;
    }

    public String getName()
    {
        return name;
    }

    public Type getType()
    {
        return type;
    }
}
