package tools;

import tools.types.Type;

import java.io.Serializable;

public class Variable implements Serializable
{
    String name;
    Type value;

    public Variable(String name)
    {
        this.name = name;
        this.value = null;
    }

    public String getName()
    {
        return name;
    }

    public Type getValue()
    {
        return value;
    }
}
