package forms;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TabComponent extends JPanel implements ActionListener
{
    private final MainFrame mainWindow;
    private final JTabbedPane pane;
    private JButton button;

    public TabComponent(final MainFrame mainWindow, final JTabbedPane pane)
    {
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        this.mainWindow = mainWindow;
        this.pane = pane;

        setOpaque(false);
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        JLabel label = new JLabel()
        {
            public String getText()
            {
                int i = pane.indexOfTabComponent(TabComponent.this);
                if (i != -1)
                    return pane.getTitleAt(i);
                return null;
            }
        };
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

        button = new JButton(new ImageIcon("resources/closetab.png"));
        button.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        button.setPreferredSize(new Dimension(13, 13));
        button.setMaximumSize(new Dimension(13, 13));
        button.addActionListener(this);

        add(label);
        add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == button)
        {
            int i = pane.indexOfTabComponent(this);
            if (i != -1)
                mainWindow.closeTabPane(i);
        }
    }
}
