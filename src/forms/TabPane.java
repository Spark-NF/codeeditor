package forms;

import project.Project;
import project.ProjectPanel;
import tools.exps.Exp;
import tools.exps.GetExp;
import tools.exps.IfExp;
import tools.exps.SetExp;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

public class TabPane extends JPanel implements ActionListener
{
    Project project;
    ProjectPanel projectPanel;
    MainFrame mainFrame;

    JButton buttonHistoryUndoAll;
    JButton buttonHistoryUndo;
    JButton buttonHistoryRedo;
    JButton buttonHistoryRedoAll;

    ArrayList<Exp> exps;
    ArrayList<JToggleButton> toolButtons;
    Exp currentExp;
    JPanel history;
    JScrollPane scrollPaneHistory;
    JScrollPane scrollArea;

    public TabPane(Project project, MainFrame mainFrame)
    {
        this.project = project;
        this.projectPanel = new ProjectPanel(project);
        this.mainFrame = mainFrame;

        JPanel toolBarTop = new JPanel();
        toolBarTop.setLayout(new BoxLayout(toolBarTop, BoxLayout.PAGE_AXIS));
        JPanel toolBar = new JPanel();
        toolBar.setLayout(new BoxLayout(toolBar, BoxLayout.PAGE_AXIS));
        toolBar.setBorder(BorderFactory.createEmptyBorder());
        toolButtons = new ArrayList<JToggleButton>();
        exps = new ArrayList<Exp>();
        exps.add(new IfExp());
        exps.add(new GetExp());
        exps.add(new SetExp());
        for (Exp exp : exps)
        {
            JToggleButton button = new JToggleButton(exp.getName());
            if (new File("resources/tools/" + exp.getClass().getSimpleName() + ".png").isFile())
            {
                button.setIcon(new ImageIcon("resources/tools/" + exp.getClass().getSimpleName() + ".png"));
                button.setToolTipText(exp.getName());
                button.setText("");
                button.setPreferredSize(new Dimension(20, button.getPreferredSize().height));
            }
            button.setFocusPainted(false);
            button.addActionListener(this);
            button.setBorder(BorderFactory.createEmptyBorder(7, 7, 5, 7));
            toolButtons.add(button);
            toolBar.add(button);
        }
        toolButtons.get(0).setSelected(true);
        toolBarTop.add(toolBar);
        toolBarTop.add(Box.createVerticalGlue());
        toolBarTop.setBorder(BorderFactory.createEmptyBorder(0, 2, 2, 3));

        Border border = BorderFactory.createEmptyBorder(4, 12, 2, 12);
        buttonHistoryUndoAll = new JButton(new ImageIcon("resources/undoall.png"));
        buttonHistoryUndoAll.setToolTipText("Undo all");
        buttonHistoryUndoAll.setBorder(border);
        buttonHistoryUndoAll.addActionListener(this);
        buttonHistoryUndoAll.setFocusPainted(false);
        buttonHistoryUndo = new JButton(new ImageIcon("resources/undo.png"));
        buttonHistoryUndo.setBorder(BorderFactory.createEmptyBorder(4, 12, 2, 11));
        buttonHistoryUndo.setToolTipText("Undo");
        buttonHistoryUndo.addActionListener(this);
        buttonHistoryUndo.setFocusPainted(false);
        buttonHistoryRedo = new JButton(new ImageIcon("resources/redo.png"));
        buttonHistoryRedo.setBorder(BorderFactory.createEmptyBorder(4, 12, 2, 11));
        buttonHistoryRedo.setToolTipText("Redo");
        buttonHistoryRedo.addActionListener(this);
        buttonHistoryRedo.setFocusPainted(false);
        buttonHistoryRedoAll = new JButton(new ImageIcon("resources/redoall.png"));
        buttonHistoryRedoAll.setToolTipText("Redo all");
        buttonHistoryRedoAll.setBorder(border);
        buttonHistoryRedoAll.addActionListener(this);
        buttonHistoryRedoAll.setFocusPainted(false);

        JPanel actionBar = new JPanel();
        actionBar.setLayout(new BoxLayout(actionBar, BoxLayout.LINE_AXIS));
        actionBar.add(buttonHistoryUndoAll);
        actionBar.add(buttonHistoryUndo);
        actionBar.add(buttonHistoryRedo);
        actionBar.add(buttonHistoryRedoAll);
        actionBar.setBorder(BorderFactory.createEmptyBorder());

        JPanel historyTop = new JPanel();
        historyTop.setLayout(new BoxLayout(historyTop, BoxLayout.PAGE_AXIS));
        history = new JPanel();
        history.setLayout(new GridBagLayout());
        history.setBorder(BorderFactory.createEmptyBorder());

        JPanel hTop = new JPanel();
        hTop.setLayout(new BorderLayout());
        hTop.add(history, BorderLayout.PAGE_START);

        scrollPaneHistory = new JScrollPane(hTop);
        scrollPaneHistory.setBorder(BorderFactory.createEmptyBorder());
        scrollPaneHistory.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPaneHistory.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPaneHistory.getVerticalScrollBar().setUnitIncrement(16);
        scrollPaneHistory.getHorizontalScrollBar().setUnitIncrement(16);
        scrollPaneHistory.revalidate();
        scrollPaneHistory.repaint();
        scrollPaneHistory.setAlignmentX(0.0f);

        actionBar.setAlignmentX(0.0f);
        historyTop.add(scrollPaneHistory);
        historyTop.add(actionBar);
        historyTop.setBorder(BorderFactory.createEmptyBorder(0, 3, 3, 3));

        scrollArea = new JScrollPane(projectPanel);
        scrollArea.setBorder(BorderFactory.createEmptyBorder());
        scrollArea.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollArea.getVerticalScrollBar().setUnitIncrement(16);
        scrollArea.getHorizontalScrollBar().setUnitIncrement(16);
        scrollArea.revalidate();
        scrollArea.repaint();

        setLayout(new BorderLayout());
        add(toolBarTop, BorderLayout.WEST);
        add(scrollArea, BorderLayout.CENTER);
        add(historyTop, BorderLayout.EAST);

        currentExp = exps.get(0);
    }

    public void save()
    {
        project.save();
    }
    public void saveAs()
    {
        project.saveAs();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() instanceof JToggleButton && toolButtons.contains(e.getSource()))
        {
            for (JToggleButton b : toolButtons)
                b.setSelected(b == e.getSource());
            currentExp = exps.get(toolButtons.indexOf(e.getSource()));
        }
    }
}
