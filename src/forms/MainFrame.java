package forms;

import project.Project;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.prefs.Preferences;

public class MainFrame extends JFrame implements ActionListener
{
    JTabbedPane tabbedPane;
    ArrayList<TabPane> tabPanes;
    Preferences prefs;

    private JMenuItem menuFileNew;
    private JMenuItem menuFileOpen;
    private JMenuItem menuFileClose;
    private JMenuItem menuFileSave;
    private JMenuItem menuFileSaveAs;
    private JMenuItem menuFilePrint;
    private JMenuItem menuFileExit;
    private JMenuItem menuEditUndo;
    private JMenuItem menuEditRedo;
    private JMenuItem menuDisplayZoomIn;
    private JMenuItem menuDisplayZoomOut;
    private JMenuItem menuDisplayFitWindow;
    private JMenuItem menuDisplayRealSize;
    private JMenuItem menuEditEraseSelection;
    private JMenuItem menuEditSelectAll;
    private JMenuItem menuEditUnselect;
    private JMenuItem menuEditCopy;
    private JMenuItem menuEditCut;
    private JMenuItem menuEditPaste;
    private JMenuItem menuHelpAbout;

    public MainFrame()
    {
        prefs = Preferences.userRoot().node(this.getClass().getName());

        setTitle("CodeEditor");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setIconImage(new ImageIcon("resources/icon.png").getImage());

        tabbedPane = new JTabbedPane();
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
        tabPanes = new ArrayList<TabPane>();

        setLocation(prefs.getInt("WindowPositionX", 20), prefs.getInt("WindowPositionY", 20));
        setSize(prefs.getInt("WindowWidth", 800), prefs.getInt("WindowHeight", 600));
        setExtendedState(prefs.getInt("WindowState", 0));
        if ((getExtendedState() & Frame.MAXIMIZED_BOTH) != 0)
            setSize(800, 600);
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent)
            {
                prefs.putInt("WindowPositionX", getLocation().x);
                prefs.putInt("WindowPositionY", getLocation().y);
                prefs.putInt("WindowWidth", getWidth());
                prefs.putInt("WindowHeight", getHeight());
                prefs.putInt("WindowState", getExtendedState());
            }
        });

        tabbedPane.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent changeEvent)
            {
                JTabbedPane sourceTabbedPane = (JTabbedPane)changeEvent.getSource();
                if (sourceTabbedPane.getTabCount() != 0)
                {
                    int index = sourceTabbedPane.getSelectedIndex();
                    setTitle(sourceTabbedPane.getTitleAt(index) + " - CodeEditor");
                }
                else
                    setTitle("CodeEditor");
            }
        });

        makeMenu();
    }

    private void makeMenu()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;

        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("");
        menuBar.add(menu);
        menuFileNew = new JMenuItem("New...", KeyEvent.VK_N);
        menuFileNew.setIcon(new ImageIcon("resources/new.png"));
        menuFileNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        menuFileNew.getAccessibleContext().setAccessibleDescription("");
        menuFileNew.addActionListener(this);
        menu.add(menuFileNew);
        menuFileOpen = new JMenuItem("Open...", KeyEvent.VK_O);
        menuFileOpen.setIcon(new ImageIcon("resources/open.png"));
        menuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        menuFileOpen.getAccessibleContext().setAccessibleDescription("");
        menuFileOpen.addActionListener(this);
        menu.add(menuFileOpen);
        menuFileClose = new JMenuItem("Close", KeyEvent.VK_C);
        menuFileClose.setIcon(new ImageIcon("resources/close.png"));
        menuFileClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
        menuFileClose.getAccessibleContext().setAccessibleDescription("");
        menuFileClose.addActionListener(this);
        menuFileClose.setEnabled(false);
        menu.add(menuFileClose);
        menu.addSeparator();
        menuFileSave = new JMenuItem("Save", KeyEvent.VK_S);
        menuFileSave.setIcon(new ImageIcon("resources/save.png"));
        menuFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        menuFileSave.getAccessibleContext().setAccessibleDescription("");
        menuFileSave.addActionListener(this);
        menuFileSave.setEnabled(false);
        menu.add(menuFileSave);
        menuFileSaveAs = new JMenuItem("Save as...", KeyEvent.VK_S);
        menuFileSaveAs.setIcon(new ImageIcon("resources/saveas.png"));
        menuFileSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
        menuFileSaveAs.getAccessibleContext().setAccessibleDescription("");
        menuFileSaveAs.addActionListener(this);
        menuFileSaveAs.setEnabled(false);
        menu.add(menuFileSaveAs);
        menu.addSeparator();
        menuFilePrint = new JMenuItem("Print...", KeyEvent.VK_P);
        menuFilePrint.setIcon(new ImageIcon("resources/print.png"));
        menuFilePrint.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
        menuFilePrint.getAccessibleContext().setAccessibleDescription("");
        menuFilePrint.addActionListener(this);
        menuFilePrint.setEnabled(false);
        menu.add(menuFilePrint);
        menu.addSeparator();
        menuFileExit = new JMenuItem("Exit", KeyEvent.VK_E);
        menuFileExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        menuFileExit.getAccessibleContext().setAccessibleDescription("");
        menuFileExit.addActionListener(this);
        menu.add(menuFileExit);

        menu = new JMenu("Edit");
        menu.setMnemonic(KeyEvent.VK_E);
        menu.getAccessibleContext().setAccessibleDescription("");
        menuBar.add(menu);
        menuEditUndo = new JMenuItem("Undo", KeyEvent.VK_U);
        menuEditUndo.setIcon(new ImageIcon("resources/undo.png"));
        menuEditUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
        menuEditUndo.getAccessibleContext().setAccessibleDescription("");
        menuEditUndo.addActionListener(this);
        menu.add(menuEditUndo);
        menuEditRedo = new JMenuItem("Redo", KeyEvent.VK_R);
        menuEditRedo.setIcon(new ImageIcon("resources/redo.png"));
        menuEditRedo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
        menuEditRedo.getAccessibleContext().setAccessibleDescription("");
        menuEditRedo.addActionListener(this);
        menu.add(menuEditRedo);
        menu.addSeparator();
        menuEditCopy = new JMenuItem("Copy", KeyEvent.VK_C);
        menuEditCopy.setIcon(new ImageIcon("resources/copy.png"));
        menuEditCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        menuEditCopy.getAccessibleContext().setAccessibleDescription("");
        menuEditCopy.addActionListener(this);
        menu.add(menuEditCopy);
        menuEditCut = new JMenuItem("Cut", KeyEvent.VK_T);
        menuEditCut.setIcon(new ImageIcon("resources/cut.png"));
        menuEditCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        menuEditCut.getAccessibleContext().setAccessibleDescription("");
        menuEditCut.addActionListener(this);
        menu.add(menuEditCut);
        menuEditPaste = new JMenuItem("Paste", KeyEvent.VK_P);
        menuEditPaste.setIcon(new ImageIcon("resources/paste.png"));
        menuEditPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
        menuEditPaste.getAccessibleContext().setAccessibleDescription("");
        menuEditPaste.addActionListener(this);
        menu.add(menuEditPaste);
        menu.addSeparator();
        menuEditEraseSelection = new JMenuItem("Clear selection", KeyEvent.VK_C);
        menuEditEraseSelection.setIcon(new ImageIcon("resources/clearselection.png"));
        menuEditEraseSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        menuEditEraseSelection.getAccessibleContext().setAccessibleDescription("");
        menuEditEraseSelection.addActionListener(this);
        menu.add(menuEditEraseSelection);
        menuEditSelectAll = new JMenuItem("Select all", KeyEvent.VK_A);
        menuEditSelectAll.setIcon(new ImageIcon("resources/selectall.png"));
        menuEditSelectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        menuEditSelectAll.getAccessibleContext().setAccessibleDescription("");
        menuEditSelectAll.addActionListener(this);
        menu.add(menuEditSelectAll);
        menuEditUnselect = new JMenuItem("Unselect", KeyEvent.VK_L);
        menuEditUnselect.setIcon(new ImageIcon("resources/unselect.png"));
        menuEditUnselect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
        menuEditUnselect.getAccessibleContext().setAccessibleDescription("");
        menuEditUnselect.addActionListener(this);
        menu.add(menuEditUnselect);

        menu = new JMenu("Display");
        menu.setMnemonic(KeyEvent.VK_D);
        menu.getAccessibleContext().setAccessibleDescription("");
        menuBar.add(menu);
        menuDisplayZoomIn = new JMenuItem("Zoom in", KeyEvent.VK_I);
        menuDisplayZoomIn.setIcon(new ImageIcon("resources/zoomin.png"));
        menuDisplayZoomIn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, InputEvent.CTRL_MASK));
        menuDisplayZoomIn.getAccessibleContext().setAccessibleDescription("");
        menuDisplayZoomIn.addActionListener(this);
        menu.add(menuDisplayZoomIn);
        menuDisplayZoomOut = new JMenuItem("Zoom out", KeyEvent.VK_Z);
        menuDisplayZoomOut.setIcon(new ImageIcon("resources/zoomout.png"));
        menuDisplayZoomOut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, InputEvent.CTRL_MASK));
        menuDisplayZoomOut.getAccessibleContext().setAccessibleDescription("");
        menuDisplayZoomOut.addActionListener(this);
        menu.add(menuDisplayZoomOut);
        menuDisplayFitWindow = new JMenuItem("Fit window", KeyEvent.VK_Z);
        menuDisplayFitWindow.setIcon(new ImageIcon("resources/fitwindow.png"));
        menuDisplayFitWindow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
        menuDisplayFitWindow.getAccessibleContext().setAccessibleDescription("");
        menuDisplayFitWindow.addActionListener(this);
        menu.add(menuDisplayFitWindow);
        menuDisplayRealSize = new JMenuItem("Real size", KeyEvent.VK_R);
        menuDisplayRealSize.setIcon(new ImageIcon("resources/realsize.png"));
        menuDisplayRealSize.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, InputEvent.CTRL_MASK));
        menuDisplayRealSize.getAccessibleContext().setAccessibleDescription("");
        menuDisplayRealSize.addActionListener(this);
        menu.add(menuDisplayRealSize);

        menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_H);
        menu.getAccessibleContext().setAccessibleDescription("");
        menuBar.add(menu);
        menuHelpAbout = new JMenuItem("About", KeyEvent.VK_A);
        menuHelpAbout.getAccessibleContext().setAccessibleDescription("");
        menuHelpAbout.addActionListener(this);
        menu.add(menuHelpAbout);

        setJMenuBar(menuBar);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == menuFileExit)
            System.exit(0);
        else if (e.getSource() == menuHelpAbout)
            new AboutDialog(this);
        else if (e.getSource() == menuFileOpen)
        {
            JFileChooser fc = new JFileChooser();
            fc.setMultiSelectionEnabled(true);
            fc.setFileFilter(new FileNameExtensionFilter("CodeEditor project", "ce"));
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION)
                for (File f : fc.getSelectedFiles())
                    addTabPane(Project.load(f.getAbsolutePath()));
        }
        else if (e.getSource() == menuFileNew)
            addTabPane(new Project());
        else if (e.getSource() == menuFileSave)
            if (tabbedPane.getTabCount() > 0)
                tabPanes.get(tabbedPane.getSelectedIndex()).save();
        else if (e.getSource() == menuFileSaveAs)
            if (tabbedPane.getTabCount() > 0)
                tabPanes.get(tabbedPane.getSelectedIndex()).saveAs();
        /*else if (e.getSource() == menuFilePrint)
            if (tabbedPane.getTabCount() > 0)
                tabPanes.get(tabbedPane.getSelectedIndex()).print();*/
        else if (e.getSource() == menuFileClose)
            if (tabbedPane.getTabCount() > 0)
                closeTabPane(tabbedPane.getSelectedIndex());
    }

    public void addTabPane(Project project)
    {
        TabPane pane = new TabPane(project, this);
        tabPanes.add(pane);

        tabbedPane.addTab(project.getName(), null, pane, "");
        tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
        tabbedPane.setTabComponentAt(tabbedPane.getTabCount() - 1, new TabComponent(this, tabbedPane));

        if (!menuFileClose.isEnabled())
        {
            menuFileClose.setEnabled(true);
            menuFileSave.setEnabled(true);
            menuFileSaveAs.setEnabled(true);
            menuFilePrint.setEnabled(true);
        }
    }

    public void closeTabPane(int index)
    {
        tabPanes.remove(index);
        tabbedPane.remove(index);

        if (tabbedPane.getTabCount() == 0)
        {
            menuFileClose.setEnabled(false);
            menuFileSave.setEnabled(false);
            menuFileSaveAs.setEnabled(false);
            menuFilePrint.setEnabled(false);
        }
    }
}
