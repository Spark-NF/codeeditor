package forms;

import javax.swing.*;
import java.awt.*;

public class AboutDialog extends JDialog
{
    public AboutDialog(JFrame parent)
    {
        setTitle("About");
        setModal(true);
        pack();
        setMinimumSize(new Dimension(200, 100));
        setLocationRelativeTo(parent);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        getRootPane().setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));

        add(new JLabel("<html>CodeEditor has been made by Nicolas Faure.</html>"));

        setVisible(true);
    }
}
