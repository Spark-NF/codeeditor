import forms.MainFrame;
import project.Project;

import javax.swing.*;
import java.awt.*;

public class Main
{
    public static void main(String[] args)
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.put("TabbedPane.focus", new Color(0, 0, 0, 0));
            UIManager.put("SplitPane.border", BorderFactory.createEmptyBorder());
        }
        catch (Exception ignored)
        {}

        MainFrame mf = new MainFrame();
        for (String file : args)
            mf.addTabPane(Project.load(file));
        mf.addTabPane(new Project());
        mf.setVisible(true);
    }
}
