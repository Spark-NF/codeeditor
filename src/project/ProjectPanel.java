package project;

import tools.exps.Exp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class ProjectPanel extends JPanel implements MouseListener, MouseMotionListener
{
    Project project;
    int decX;
    int decY;
    int pressedX;
    int pressedY;

    public ProjectPanel(Project project)
    {
        this.project = project;

        setOpaque(true);
        setBackground(new Color(192, 192, 192, 255));
        setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        addMouseListener(this);
        addMouseMotionListener(this);

        decX = -99;
        decY = -99;
    }

    @Override
    public void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D)graphics;

        g.setColor(new Color(160, 160, 160, 255));
        for (int x = (decX / 10) * 10; x < getWidth() + decX; x += 10)
            g.drawLine(x - decX, 0, x - decX, getHeight());
        for (int y = (decY / 10) * 10; y < getHeight() + decY; y += 10)
            g.drawLine(0, y - decY, getWidth(), y - decY);

        g.setColor(new Color(128, 128, 128, 255));
        for (int x = (decX / 100) * 100; x < getWidth() + decX; x += 100)
            g.drawLine(x - decX, 0, x - decX, getHeight());
        for (int y = (decY / 100) * 100; y < getHeight() + decY; y += 100)
            g.drawLine(0, y - decY, getWidth(), y - decY);

        g.setColor(new Color(192, 64, 64, 255));
        g.drawLine(-decX, 0, -decX, getHeight());
        g.drawLine(0, -decY, getWidth(), -decY);

        for (Exp t : project.getExps())
            t.draw(g, decX, decY);
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        pressedX = e.getX();
        pressedY = e.getY();
    }

    @Override
    public void mouseDragged(MouseEvent e)
    {
        decX -= e.getX() - pressedX;
        decY -= e.getY() - pressedY;

        pressedX = e.getX();
        pressedY = e.getY();

        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e)
    {}
    @Override
    public void mouseReleased(MouseEvent e)
    {}
    @Override
    public void mouseEntered(MouseEvent e)
    {}
    @Override
    public void mouseExited(MouseEvent e)
    {}
    @Override
    public void mouseClicked(MouseEvent e)
    {}
}
