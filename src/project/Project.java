package project;

import tools.exps.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Project implements Serializable
{
    transient String filename;
    String name;
    private ArrayList<Exp> exps;

    public Project()
    {
        filename = "";
        name = "New project";
        exps = new ArrayList<Exp>();

        exps.add(new StartExp());
        exps.add(new GetExp());
        exps.add(new IfExp());
        exps.add(new SetExp());

        exps.get(0).setPosition(0, 0);
        exps.get(1).setPosition(0, 50);
        exps.get(2).setPosition(200, 0);
        exps.get(3).setPosition(400, 0);
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
        if (name.isEmpty())
            name = new File(filename).getName();
    }

    public static Project load(String filename)
    {
        try
        {
            FileInputStream fis = new FileInputStream(filename);
            GZIPInputStream gis = new GZIPInputStream(fis);
            ObjectInputStream ois = new ObjectInputStream(gis);
            Project ret = (Project)ois.readObject();
            ret.setFilename(filename);
            ois.close();
            return ret;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void saveAs()
    {
        String fn = "";
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("CodeEditor project", "ce"));
        int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            fn = fc.getSelectedFile().getAbsolutePath();
            int i = fn.lastIndexOf('.');
            if (i <= 0)
                fn += "." + ((FileNameExtensionFilter)fc.getFileFilter()).getExtensions()[0];
        }
        save(fn);
    }
    public void save()
    {
        if (filename.isEmpty())
            saveAs();
        else
            save(filename);
    }
    public void save(String filename)
    {
        if (filename.isEmpty())
            return;
        try
        {
            FileOutputStream fos = new FileOutputStream(filename);
            GZIPOutputStream gos = new GZIPOutputStream(fos);
            ObjectOutputStream oos = new ObjectOutputStream(gos);
            oos.writeObject(this);
            oos.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getName()
    {
        return name;
    }

    public Iterable<Exp> getExps()
    {
        return exps;
    }

    public String getFilename()
    {
        return filename;
    }
}
